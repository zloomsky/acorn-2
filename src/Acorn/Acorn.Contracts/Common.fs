﻿namespace Acorn.Contracts

open System

type Init() = class end

type IEvent = interface end

type ICommand = interface end

type IQuery = interface end 

type Envelope = 
    {Id: Guid; Message: obj}

type StoreBox() =
    member val Data = String.Empty with get, set
    member val CreatedAt = DateTime.UtcNow with get, set
    member val Id = Guid.Empty with get, set
    member val Type = String.Empty with get, set

type Failed =    
    {Message: string}
    interface IEvent
