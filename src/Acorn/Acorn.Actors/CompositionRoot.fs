﻿namespace Acorn.Actors

open Ninject.Modules
open Orleankka

type IActorsMark = interface end

type ActorsModule() =
    inherit NinjectModule()

    override this.Load() =            
        for t in this.GetType().Assembly.GetTypes() do
            if typeof<Actor>.IsAssignableFrom t then
                this.Bind(t).ToSelf() |> ignore