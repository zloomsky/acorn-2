﻿namespace Acorn.Actors

open Orleankka
open System
open System.Threading.Tasks
open Acorn.Infrastructure
open Acorn.Contracts



[<StreamSubscription(Source = ProviderName + ":" + Pipes.Init, Target = Pipes.Init)>]
type Scheduler(id, runtime, env: IEnvironment) =
    inherit Actor(id, runtime) 


    override __.OnReminder id = Task.FromResult(ignore()) :> Task    

   
    member __.Handle (init: Init) = ()  
        
        
      

