﻿namespace Acorn.Api

open System
open System.Linq
open System.Web.Http
open System.Threading.Tasks
open Microsoft.Owin


[<RoutePrefix("api/hello")>]
type TestController() = 
    inherit ApiController()   
    
    [<HttpGet>]     
    [<Route("")>]     
    member __.Get() = 
        "Hello!"

   

