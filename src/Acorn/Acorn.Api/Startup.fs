﻿namespace Acorn.Api


open Owin
open Newtonsoft.Json.Serialization        
open System
open System.Web.Http
open Ninject
open Ninject.Web.Common.OwinHost
open Ninject.Web.WebApi.OwinHost  
open NLog.Targets
open NLog
open Acorn.Infrastructure



type Config = {
    Id : RouteParameter
}

type Startup() =   
    let logs = [
        new LogentriesTarget(Name = "LE", Token = connectionString LogentriesToken, Layout = layout)
    ]     

    member __.Configuration(app: IAppBuilder) = 
        LogManager.Configuration <- logConfig logs 

        let kernel = new StandardKernel(new ApiModule(), new InfrastructureModule()) :> IKernel   
        let config = new HttpConfiguration()
        config.Formatters.Remove config.Formatters.XmlFormatter |> ignore 
        config.Formatters.JsonFormatter.SerializerSettings.ContractResolver <- DefaultContractResolver() 
        config.MapHttpAttributeRoutes()
        config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", { Id = RouteParameter.Optional }) |> ignore          
      
        app.UseNinjectMiddleware(fun () -> kernel) |> ignore
        app.UseNinjectWebApi(config) |> ignore   
            
           
       
            
    
   
