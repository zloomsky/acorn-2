﻿namespace Acorn.Api

open Ninject.Modules
open Orleankka

type ApiModule() =
    inherit NinjectModule()
    override this.Load() = 
        this.Bind<IActorSystem>().To<ActorSystemClient>().InSingletonScope() |> ignore          