﻿namespace Acorn.Api

open System
open System.Reflection
open Orleans.Runtime.Configuration
open Orleans
open Orleans.Providers.Streams.SimpleMessageStream
open Orleans.Runtime
open Orleans.Runtime.Configuration
open Orleankka
open Orleankka.Core
open Orleankka.Client
open Acorn.Infrastructure
open Acorn.Actors
open NLog.Targets


type ActorSystemClient() =    

    let clientConfig() =                      
        TraceLogger.LogConsumers.Add(NLogConsumer())         
        let clientConfig = ClientConfiguration().LoadFromEmbeddedResource(Assembly.GetExecutingAssembly(), "Client.xml")
        clientConfig.DataConnectionString <- connectionString SqlConnectionString 
        clientConfig.DeploymentId <- connectionString ClusterId    
        clientConfig 
          
    let configuration config = 
        ActorSystem.Configure()
                    .Client()
                    .From(config)
                    .Serializer<BinarySerializer>()
                    .Register<SimpleMessageStreamProvider>(ProviderName)
                    .Register(typeof<IActorsMark>.Assembly)
    
    let mutable _client : IActorSystem = null

    let mutable _config : ClientConfigurator = null

    member __.Config
        with get() = 
            if isNull _config then
                _config <- clientConfig() |> configuration
            _config

    member this.Client 
        with get() =
            if isNull _client then
                _client <- this.Config.Done()
            _client

    member private this.Try (action: IActorSystem -> 'a) =
        let rec withTry action attempt =                 
            try
                action(this.Client)
            with e ->
                if attempt > 5 then raise e
                this.DisposeClient()                
                withTry action (attempt + 1)
        withTry action 1

    member __.DisposeClient() = 
        if not(isNull _client) then
            _client.Dispose()
            _client <- null
        if not(isNull _config) then
            _config.Dispose()
            _config <-null

    interface IActorSystem with
        member this.ActorOf(t,id) = 
            this.Try (fun c -> c.ActorOf(t,id))
        member this.ActorOf path =
            this.Try(fun c-> c.ActorOf path)
        member this.StreamOf path =
            this.Try(fun c-> c.StreamOf path)


    interface IDisposable with
        member this.Dispose() =
            this.DisposeClient()   