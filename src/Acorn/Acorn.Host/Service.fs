﻿namespace Acorn.Host

open System.Reflection
open Orleans
open Orleans.Providers.Streams.SimpleMessageStream
open Orleans.Runtime
open Orleans.Runtime.Configuration
open Orleankka
open Orleankka.Core
open Orleankka.Cluster
open Ninject
open Ninject.Parameters
open Ninject.Modules
open Acorn.Actors
open Acorn.Contracts
open Acorn.Infrastructure  
open NLog.Targets
open NLog.Fluent
open Topshelf
  

type NinjectActivator() =
        inherit ActorActivator()

        let container = new StandardKernel(new ActorsModule(), new InfrastructureModule())

        override __.Activate (actorType, id, runtime) =             
            let idParameter = ConstructorArgument("id", id)
            let runtimeParameter = ConstructorArgument("runtime", runtime)
            container.Get(actorType, idParameter, runtimeParameter) :?> Actor 


type InitBootstrapper() =
    interface IBootstrapper with
        member __.Run _ = 
            ClusterActorSystem.Current.ActorOf<Scheduler>(Pipes.Init).Tell(Init()) 
            |> ignore                  
            TaskDone.Done

               
type HostService() =
    let mutable system: IActorSystem = null

    let createSystem() =
        TraceLogger.LogConsumers.Add(NLogConsumer())  
        let clusterConfig = ClusterConfiguration().LoadFromEmbeddedResource(Assembly.GetExecutingAssembly(), "Host.xml")  
        clusterConfig.Globals.DataConnectionString <- connectionString SqlConnectionString
        clusterConfig.Globals.DeploymentId <- connectionString ClusterId      
        clusterConfig.Globals.AdoInvariant <- "Npgsql"    
           
        system <- ActorSystem.Configure()
                                .Cluster()                                 
                                .From(clusterConfig)
                                .Serializer<BinarySerializer>()
                                .Register(typeof<IActorsMark>.Assembly)                                
                                .Register<SimpleMessageStreamProvider>(ProviderName)
                                .Activator<NinjectActivator>()
                                .Run<InitBootstrapper>()
                                .Done()  

    interface ServiceControl with
        member __.Start _ =
            try
                createSystem()
                Log.Info().Message("Host service started.").Write()
                true
            with e ->                
                Log.Error().Message("Failed to start host service.").Exception(e).Write()
                false

        member __.Stop _ =
            if not (isNull system)
            then
                system.Dispose()
                system <- null
                Log.Info().Message("Host service stopped.").Write()
                true
            else
                false