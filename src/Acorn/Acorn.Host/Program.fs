﻿namespace Acorn.Host


open Topshelf
open Topshelf.ServiceConfigurators
open Topshelf.HostConfigurators
open Acorn.Infrastructure
open NLog.Targets
open NLog
    
open System

module Program =
       
    let logs = [
        new LogentriesTarget(Name = "LE", Token = connectionString LogentriesToken, Layout = layout, Debug = false) :> Target;
        new ColoredConsoleTarget(Name = "Console", Layout = layout) :> Target
    ]           

    let hostConfiguration (configurator : HostConfigurator) =      
        configurator
        |> fun c -> c.Service<HostService>() 
        |> fun c -> c.RunAsLocalService() 
        |> fun c -> c.EnableServiceRecovery(fun x -> x.RestartService(0) |> ignore) 
        |> tee (fun c -> c.SetServiceName("Acorn"))
        |> tee (fun c -> c.SetStartTimeout(TimeSpan.FromMinutes(2.0)))
        |> tee (fun c -> c.SetStopTimeout(TimeSpan.FromMinutes(2.0)))
        |> fun c -> c.UseNLog(new LogFactory(Configuration = logConfig logs))
       

    [<EntryPoint>]
    let main argv =                   
        LogManager.Configuration <- logConfig logs   
        int (HostFactory.Run(Action<HostConfigurator> hostConfiguration))
