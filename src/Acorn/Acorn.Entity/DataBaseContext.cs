﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acorn.Entity
{
    public class DataBaseContext : IDataBaseContext
    {
        public DataBaseContext(string connectionString)
        {

        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Get<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public IReadOnlyCollection<T> Get<T>(string sql, params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyCollection<T>> GetAsync<T>(string sql, params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public void Add<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }

        public void AddRange<T>(IEnumerable<T> items) where T : class
        {
            throw new NotImplementedException();
        }

        public void Remove<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }

        public void RemoveRange<T>(IEnumerable<T> items) where T : class
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            throw new NotImplementedException();
        }
    }
}