﻿namespace Acorn.Infrastructure

open Acorn.Entity

[<AutoOpen>]
module Environment =    

    type IEnvironment =
        abstract member GetDataContext : unit -> IDataBaseContext
        abstract member GetStream: string * string -> IEventStream

    type ActorEnvironment(connectionString) =   

        interface IEnvironment with              
           
            member __.GetDataContext() = 
                new DataBaseContext(connectionString) :> IDataBaseContext

            member __.GetStream(table, partition) = 
                new PostgresEventStream(connectionString, table, partition) :> IEventStream