﻿namespace Acorn.Infrastructure

[<AutoOpen>]
module Literals =

    [<Literal>]
    let ProviderName = "SMS"          
   
    [<Literal>]
    let SqlConnectionString = "SQLDataBase"
      
    [<Literal>]
    let ClusterId = "ClusterId"    

    [<Literal>]
    let LogentriesToken = "LogentriesToken" 

