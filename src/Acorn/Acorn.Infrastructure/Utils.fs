﻿namespace Acorn.Infrastructure

open System
open System.Configuration    
open System.Threading.Tasks
open Orleankka
open Acorn.Contracts

[<AutoOpen>]
module Utils =         

    let connectionString(name:string) =
        ConfigurationManager.ConnectionStrings.[name].ConnectionString    

    let toEnvelope message =
        {Id = Guid.NewGuid(); Message = message}  
 
    let toOption value = 
        if isNull(box value) then None else Some(value)

    let toNullable = function
    | None -> new System.Nullable<_>()
    | Some x -> new System.Nullable<_>(x)   

    let empty = String.IsNullOrEmpty

    let notEmpty = String.IsNullOrEmpty >> not 

    let compare s1 s2 = String.Compare(s1, s2, StringComparison.InvariantCultureIgnoreCase) <> 0

    //Railway

    type Result<'TSuccess,'TFailure> = 
    | Success of 'TSuccess
    | Failure of 'TFailure
   
    let succeed x = 
        Success x
    
    let fail x = 
        Failure x

    let failEvent x =        
        [{Failed.Message = x}]   

    let events successFunc failureFunc twoTrackInput =
        match twoTrackInput with
        | Success s -> (successFunc >> Seq.map (fun e -> e :> IEvent)) s
        | Failure f -> (failureFunc >> Seq.map (fun e -> e :> IEvent)) f
        
   
    let either successFunc failureFunc twoTrackInput =
        match twoTrackInput with
        | Success s -> successFunc s
        | Failure f -> failureFunc f
   
    let bind f = 
        either f fail
   
    let (>>=) x f = 
        bind f x

    let (>=>) s1 s2 = 
        s1 >> bind s2
   
    let switch f = 
        f >> succeed
   
    let map f = 
        either (f >> succeed) fail
    
    let tee f x = 
        f x; x 
    
    let tryCatch f exnHandler x =
        try
            f x |> succeed
        with
        | ex -> exnHandler ex |> fail   
   
    let doubleMap successFunc failureFunc =
        either (successFunc >> succeed) (failureFunc >> fail)
    
    let plus addSuccess addFailure switch1 switch2 x = 
        match (switch1 x),(switch2 x) with
        | Success s1,Success s2 -> Success (addSuccess s1 s2)
        | Failure f1,Success _  -> Failure f1
        | Success _ ,Failure f2 -> Failure f2
        | Failure f1,Failure f2 -> Failure (addFailure f1 f2)

    let pass v x = 
        match x with       
        | Failure x -> fail x
        | Success _ -> succeed v

    
        
        