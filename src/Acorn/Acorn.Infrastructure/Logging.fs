﻿namespace Acorn.Infrastructure

open Orleans.Runtime
open System.Reflection
open NLog
open NLog.Config
open NLog.Layouts
open NLog.Fluent
open NLog.Targets

[<AutoOpen>]
module Logging =    

    type NLogConsumer() =   
        let ignoredInfo code = code <> 100402

        interface ILogConsumer with      
            member __.Log(severity, logger, caller, message, endpoint, ex, code) =                
                let message = 
                    sprintf "endpoint: %O caller: %s code: %i message: %s" endpoint caller code message
                
                let logEx (builder : LogBuilder) = 
                    if isNull ex then builder else builder.Exception(ex)
                
                let log = 
                    match severity with
                    | Severity.Error -> Some (Log.Error().Message >> logEx)
                    | Severity.Warning -> Some (Log.Warn().Message)
                    | Severity.Info when not (ignoredInfo code)  -> Some (Log.Info().Message)
                    | _ -> None
                   

                match log with
                | Some log -> log(message).Write()
                | _ -> ignore()  
   
    let layout = 
        let version = Assembly.GetExecutingAssembly().GetName().Version
        let layout = sprintf "${date:format=yyyy.MM.dd HH\:mm\:ss.fff zzz} %O ${uppercase:${level}} ${message} ${exception:maxInnerExceptionLevel=15}" version       
        Layout.FromString(layout)

    let logConfig targets  = 
        let config = LoggingConfiguration()   
        let addTarget target =             
            config.LoggingRules.Add(LoggingRule("*", LogLevel.Info, target))
            config.AddTarget(target)
         
        Seq.iter addTarget targets
        config    