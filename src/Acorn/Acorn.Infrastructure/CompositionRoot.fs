﻿namespace Acorn.Infrastructure

open Ninject.Modules

type InfrastructureModule() =
    inherit NinjectModule()        
    override this.Load() =            
        this.Bind<IEnvironment>().To<ActorEnvironment>().WithConstructorArgument("connectionString", connectionString SqlConnectionString)            
        |> ignore

