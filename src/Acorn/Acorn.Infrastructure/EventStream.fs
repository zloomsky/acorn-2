﻿namespace Acorn.Infrastructure

open System
open System.Threading.Tasks
open Acorn.Contracts

[<AutoOpen>]
module EventStream =     
   
    type IEventStream =      
        abstract member ReadAll : unit -> seq<IEvent>
        abstract member Write : 'a -> Task
        abstract member WriteSeq : seq<'a> -> Task   

    type PostgresEventStream(connectionString, tableName, partitionName) =    
        interface IEventStream with
            member __.WriteSeq events = failwith "not implemented"                 
            
            member __.Write event = failwith "not implemented"           
                          
            member __.ReadAll() = failwith "not implemented"         